var dtServices = angular.module('dtServices', []);

dtServices.service("OfficeService", function($http) {

   this.save = function(office) {
      return $http.post('/offices/add', office);
   };

   this.getOfficesForOrganization = function(id) {
      return $http.get('/offices/getfororganization/' + id);
   };
   
   this.getAllOffices = function() {
      return $http.get('/offices/getall');
   };

   this.search = function(query) {
      return $http.get('/offices/search/' + query);
   };

   this.edit = function(id, office) {
      return $http.put('/offices/edit/' + id, office);
   };

   this.delete = function(id) {
      return $http.delete('/offices/delete/' + id);
   };
});

dtServices.service("OrganizationService", function($http) {

   this.save = function(organization) {
      return $http.post('/organizations/add', organization);
   };

   this.getAll = function() {
      return $http.get('/organizations/getall');
   };

   this.delete = function(id) {
      return $http.delete('/organizations/delete/' + id);
   };

   this.edit = function(id, organization) {
      return $http.put('/organizations/edit/' + id, organization);
   };

});

dtServices.service("UserService", function($http) {

   this.delete = function() {
      return $http.delete('/users/delete');
   };
});

dtServices.service("TeacherService", function($http) {
   
   this.getForOffice = function(id) {
      return $http.get('/teachers/getforoffice/' + id);
   };
   
   this.getAll = function() {
      return $http.get('/teachers/getall');
   };
   
   this.search = function(query) {
      return $http.get('/teachers/search/' + query);
   };
   
   this.save = function(teacher) {
      return $http.post('/teachers/add', teacher);
   };

   this.delete = function(id) {
      return $http.delete('/teachers/delete/' + id);
   };

   this.edit = function(id, teacher) {
      return $http.put('/teachers/edit/' + id, teacher);
   };
   
   this.setNotes = function(teacher) {
      return $http.post('/teachers/setnotes', teacher);
   };
   
   this.setStatus = function(teacher) {
      return $http.post('/teachers/setstatus', teacher);
   };
   
   this.setTime = function(teacher) {
      return $http.post('/teachers/settime', teacher);
   };
    
});