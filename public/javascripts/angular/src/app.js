var dtModule = angular.module('dtModule', ['dtFilters', 'dtServices', 'ngFileUpload']);

dtModule.controller('AccountController', ['Upload', '$scope', '$http', '$timeout', '$window', 'OfficeService', 'OrganizationService', 'UserService', 'TeacherService', function(Upload, $scope, $http, $timeout, $window, OfficeService, OrganizationService, UserService, TeacherService) {

    // Setup initial module information

    $scope.allOrganizations = [];
    $scope.officesForOrganization = [];
    $scope.teachersForOffice = [];
    $scope.currentOrganization = null;
    $scope.currentOffice = null;
    $scope.currentTeacher = null;
    refreshOrganizations();

    // Logic for universities (organizations)

    $scope.openAddOrganization = function() {
        $scope.nameOrganization = '';
        $scope.adressOrganization = '';
        $scope.emailOrganization = '';
        $scope.descriptionOrganization = '';
        $('#organizationAddModal').modal('show');
    };

    $scope.openEditOrganization = function(organization) {
        $scope.editNameOrganization = organization.name;
        $scope.editAdressOrganization = organization.location;
        $scope.editEmailOrganization = organization.email;
        $scope.editDescriptionOrganization = organization.description;
        $('#organizationEditModal').modal('show');
    };

    function refreshOrganizations() {
        OrganizationService.getAll().success(function(data, status, headers, config) {
            $scope.allOrganizations = data;
            if (!$scope.currentOrganization) {
                $scope.currentOrganization = $scope.allOrganizations[0];
            }
            refreshOffices();
        }).error(function(data, status, headers, config) {
            console.log('Cant get all organizations');
        });
    }

    $scope.addOrganization = function() {
        var organization = {
            name: $scope.nameOrganization,
            location: $scope.adressOrganization,
            email: $scope.emailOrganization,
            description: $scope.descriptionOrganization
        };

        OrganizationService.save(organization).success(function(data, status, headers, config) {
            console.log('organization added');
            refreshOrganizations();
            $('#organizationAddModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant add organization');
        });
    };

    $scope.editOrganization = function() {
        var organization = {
            name: $scope.editNameOrganization,
            location: $scope.editAdressOrganization,
            email: $scope.editEmailOrganization,
            description: $scope.editDescriptionOrganization
        };

        OrganizationService.edit($scope.currentOrganization._id, organization).success(function(data, status, headers, config) {
            console.log('organization edited');
            refreshOrganizations();
            $('#organizationEditModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant edit organization');
        });
    };

    $scope.deleteOrganization = function(id) {
        OrganizationService.delete(id).success(function(data, status, headers, cofig) {
            console.log("organization deleted");
            refreshOrganizations();
        }).error(function(data, status, headers, config) {
            console.log('Cant delete organization');
        });
    };

    function findOrganizationForId(id) {
        for (var i = 0; i < $scope.allOrganizations.length; i++) {
            if ($scope.allOrganizations[i]._id == id) {
                return $scope.allOrganizations[i];
            }
        }
    }

    // Logic for departments (offices)

    $scope.openAddOffice = function() {
        if (!$scope.currentOrganization) {
            appearSwal("Warning!", "You have to select the university at first.", "warning", 2500);
            return;
        }
        $scope.nameOffice = '';
        $scope.adressOffice = '';
        $scope.emailOffice = '';
        $scope.descriptionOffice = '';
        $('#officeAddModal').modal('show');
    };

    $scope.openEditOffice = function(office) {
        $scope.editNameOffice = office.name;
        $scope.editAddressOffice = office.location;
        $scope.editEmailOffice = office.email;
        $scope.editDescriptionOffice = office.description;
        $('#officeEditModal').modal('show');
    };

    $scope.addOffice = function() {
        var office = {
            name: $scope.nameOffice,
            location: $scope.adressOffice,
            email: $scope.emailOffice,
            description: $scope.descriptionOffice,
            organizationId: $scope.currentOrganization._id
        };

        OfficeService.save(office).success(function(data, status, headers, config) {
            console.log('office added');
            refreshOffices();
            $('#officeAddModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant add office');
        });
    };

    $scope.editOffice = function() {
        var office = {
            name: $scope.editNameOffice,
            location: $scope.editAddressOffice,
            email: $scope.editEmailOffice,
            description: $scope.editDescriptionOffice
        };

        OfficeService.edit($scope.currentOffice._id, office).success(function(data, status, headers, config) {
            console.log('office edited');
            refreshOffices();
            $('#officeEditModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant edit office');
        });
    };

    function refreshOffices() {
        OfficeService.getOfficesForOrganization($scope.currentOrganization._id).success(function(data, status, headers, config) {
            $scope.officesForOrganization = data;
            if (!$scope.currentOffice && $scope.officesForOrganization[0]) {
                $scope.currentOffice = $scope.officesForOrganization[0];
            }
            refreshTeachers();
        }).error(function(data, status, headers, config) {
            console.log('Cant get oficces');
        });
    }

    $scope.getOfficesForOrganization = function(id) {
        console.log("click: " + id);
        $scope.currentOrganization = findOrganizationForId(id);
        OfficeService.getOfficesForOrganization(id).success(function(data, status, headers, config) {
            $scope.officesForOrganization = data;
            if (!$scope.currentOffice && $scope.officesForOrganization[0]) {
                $scope.currentOffice = $scope.officesForOrganization[0];
            }
            refreshTeachers();
        }).error(function(data, status, headers, config) {
            console.log('Cant get offices');
        });
    };

    $scope.deleteOffice = function(id) {
        OfficeService.delete(id).success(function(data, status, headers, cofig) {
            console.log("office deleted");
            refreshOffices();
        }).error(function(data, status, headers, config) {
            console.log('Cant delete office');
        });
    };

    function findOfficeForId(id) {
        for (var i = 0; i < $scope.officesForOrganization.length; i++) {
            if ($scope.officesForOrganization[i]._id == id) {
                return $scope.officesForOrganization[i];
            }
        }
    }

    // Logic for teachers

    $scope.openAddTeacher = function() {
        if (!$scope.currentOffice) {
            appearSwal("Warning!", "You have to select the department at first.", "warning", 2500);
            return;
        }
        $scope.firstNameTeacher = '';
        $scope.lastNameTeacher = '';
        $scope.emailTeacher = '';
        $scope.middleNameTeacher = '';
        $scope.degreeTeacher = '';
        $scope.subjectTeacher = '';
        $('#teacherAddModal').modal('show');
    };
    
    $scope.openEditTeacher = function(teacher) {
        $scope.currentTeacher = teacher;
        $scope.editFirstNameTeacher = teacher.firstName;
        $scope.editLastNameTeacher = teacher.lastName;
        $scope.editEmailTeacher = teacher.email;
        $scope.editMiddleNameTeacher = teacher.middleName;
        $scope.editDegreeTeacher = teacher.degree;
        $scope.editSubjectTeacher = teacher.subject;
        $('#teacherEditModal').modal('show');
    };

    $scope.addTeacher = function() {
        var teacher = {
            email: $scope.emailTeacher,
            firstName: $scope.firstNameTeacher,
            lastName: $scope.lastNameTeacher,
            middleName: $scope.middleNameTeacher,
            degree: $scope.degreeTeacher,
            subject: $scope.subjectTeacher,
            officeId: $scope.currentOffice._id,
            officeName: $scope.currentOffice.name,
            location: $scope.currentOffice.location
        };

        $scope.sendPhoto();

        TeacherService.save(teacher).success(function(data, status, headers, config) {
            console.log('teacher added');
            refreshTeachers();
            $('#teacherAddModal').modal('hide');
            appearSwal("Success", "Email message with password has been sent for specified address.", "success", 3500);
        }).error(function(data, status, headers, config) {
            if (status == 406) {
                appearSwal("Warning!", "This email adress has already used.", "warning", 2500);
            }
            console.log('Cant add teacher');
        });
    };
    
    $scope.editTeacher = function() {
        var teacher = {
            email: $scope.editEmailTeacher,
            firstName: $scope.editFirstNameTeacher,
            lastName: $scope.editLastNameTeacher,
            middleName: $scope.editMiddleNameTeacher,
            degree: $scope.editDegreeTeacher,
            subject: $scope.editSubjectTeacher
        };

        $scope.editSendPhoto();
        TeacherService.edit($scope.currentTeacher._id, teacher).success(function(data, status, headers, config) {
            console.log('teacher edited');
            refreshTeachers();
            $('#teacherEditModal').modal('hide');
        }).error(function(data, status, headers, config) {
            console.log('Cant edit teacher');
        });
    };

    $scope.getTeacherForOffice = function(id) {
        $scope.currentOffice = findOfficeForId(id);
        TeacherService.getForOffice(id).success(function(data, status, headers, config) {
            $scope.teachersForOffice = data;
            if (!$scope.teachersForOffice && $scope.teachersForOffice[0]) {
                $scope.currentTeacher = $scope.teachersForOffice[0];
            }
        }).error(function(data, status, headers, config) {
            console.log('Cant get teachers');
        });
    };

    function refreshTeachers() {
        TeacherService.getForOffice($scope.currentOffice._id).success(function(data, status, headers, config) {
            $scope.teachersForOffice = data;
            if (!$scope.teachersForOffice && $scope.teachersForOffice[0]) {
                $scope.currentTeacher = $scope.teachersForOffice[0];
            }
        }).error(function(data, status, headers, config) {
            console.log('Cant get teachers');
        });
    }

    $scope.deleteTeacher = function(id) {
        TeacherService.delete(id).success(function(data, status, headers, cofig) {
            console.log("teacher deleted");
            refreshTeachers();
        }).error(function(data, status, headers, config) {
            console.log('Cant delete teahcer');
        });
    };

    // Additional module logic

    function appearSwal(title, text, type, timer) {
        swal({
            title: title,
            text: text,
            type: type,
            timer: timer,
            showConfirmButton: true
        });
    }

    // Uploading file

    $scope.sendPhoto = function() { //function to call on form submit
        if ($scope.uploadFile != null)
            $scope.upload($scope.uploadFile); //call upload function
    };

    $scope.upload = function(file) {
        Upload.upload({
            url: 'https://department-table-ariandr.c9users.io/profile/' + $scope.emailTeacher, //webAPI exposed to upload the file
            data: {
                file: file
            } //pass file as data, should be user ng-model
        }).then(function(resp) { //upload function returns a promise
            $scope.uploadFile = null;
        }, function(resp) { //catch error

        }, function(evt) {
            console.log(evt);
        });
    };
    
    $scope.editSendPhoto = function() { //function to call on form submit
        if ($scope.editUploadFile != null)
            $scope.editUpload($scope.editUploadFile); //call upload function
    };
    
    $scope.editUpload = function(file) {
        Upload.upload({
            url: 'https://department-table-ariandr.c9users.io/profile/' + $scope.currentTeacher.email, //webAPI exposed to upload the file
            data: {
                file: file
            } //pass file as data, should be user ng-model
        }).then(function(resp) { //upload function returns a promise
            $scope.editUploadFile = null;
        }, function(resp) { //catch error

        }, function(evt) {
            console.log(evt);
        });
    };

}]);