var dtFilters = angular.module('dtFilters', []);

dtFilters.filter('searchForTeacher', function() {
   return function(array, searchInput) {
      if (!searchInput) {
         return array;
      }
      searchInput = searchInput.toLowerCase();
      var result = [];
      angular.forEach(array, function(teacher) {
         if (teacher.firstName.toLowerCase().indexOf(searchInput) >= 0 ||
         teacher.lastName.toLowerCase().indexOf(searchInput) >= 0 ||
         teacher.middleName.toLowerCase().indexOf(searchInput) >= 0 ||
         teacher.degree.toLowerCase().indexOf(searchInput) >= 0 ||
         teacher.subject.toLowerCase().indexOf(searchInput) >= 0) {
            result.push(teacher);
         }
      });
      return result;
   };
});

dtFilters.filter('searchFor', function() {
   return function(array, searchInput) {
      if (!searchInput) {
         return array;
      }
      searchInput = searchInput.toLowerCase();
      var result = [];
      angular.forEach(array, function(organization) {
         if (organization.name.toLowerCase().indexOf(searchInput) >= 0 ||
         organization.location.toLowerCase().indexOf(searchInput) >= 0 ||
         organization.email.toLowerCase().indexOf(searchInput) >= 0 ||(organization.description && organization.description.toLowerCase().indexOf(searchInput) >= 0)) {
            result.push(organization);
         }
      });
      return result;
   };
});