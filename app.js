var passport = require('passport');
var session = require('express-session');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var config = require('./config/configurator.js').strategies;
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var auth = require('./auth/authLogics.js');
var multer  = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, req.params.bid + ".jpeg");
  }
});

var upload = multer({ storage: storage }).single('file');


// Serialize needed user
passport.serializeUser(function(user, done) {
  done(null, user);
});

// Deserialize
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

// Google configuration
passport.use(new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL,
  passReqToCallback: true
}, auth.processAuth));

var routes = require('./routes/index');
var users = require('./routes/users');
var organizations = require('./routes/organizations');
var offices = require('./routes/offices');
var teachers = require('./routes/teachers');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bin/uploads')));
app.use(session({
  secret: 'the_secret_salt_1599',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(cookieParser());

// Enable routes
app.use('/', routes);
app.use('/users', users);
app.use('/organizations', organizations);
app.use('/offices', offices);
app.use('/teachers', teachers);

app.post('/profile/:bid', function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      res.sendStatus(400);
    } else {
      res.sendStatus(204);
    }
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
