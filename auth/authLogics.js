var User = require('../config/configurator.js').User;

function processAuth(request, accessToken, refreshToken, profile, done) {
   User.findOne({
      oauth_id: profile.id
   }, function(err, user) {
      if (err) {
         console.log(err);
      }
      if (!err && user != null) {
         done(null, user);
      }
      else {
         // Add a new user using provided profile info
         var obj = {
            oauth_id: profile.id,
            name: profile.displayName,
            email: profile.emails[0].value
         };

         var user = new User(obj);

         user.save(function(err, doc) {
            if (err || !doc) {
               console.log(err);
            }
            else {
               done(null, user);
            }
         });
      };
   });
}

module.exports.processAuth = processAuth;