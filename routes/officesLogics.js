var db = require('../config/configurator.js');

var Office = db.Office;

function getAllOffices(req, res, next) {
    Office.find({}, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc);
        }
    });
}

function searchOffices(req, res, next) {
    Office.find({}, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            var search = req.params.query.toLowerCase();
            var results = [];
            for(var i = 0; i < doc.length; i++){
                if (doc[i].name.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].location.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].email.toLowerCase().indexOf(search) >= 0 ||
                    (doc[i].description != null
                    && doc[i].description.toLowerCase().indexOf(search) >= 0)) {
                    results.push(doc[i]);
                }
            }
            res.send(results);
        }
    });
}

function getOfficeForOrganization(req, res, next) {
    var id = req.params.bid;
    Office.find({
        'organizationId': id
    }, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc);
        }
    });
}

function postOfficeAdd(req, res, next) {
    if (req.session.passport.user) {
        var officeObj = {
            name: req.body.name,
            location: req.body.location,
            description: req.body.description,
            email: req.body.email,
            organizationId: req.body.organizationId
        };
        
        var office = new Office(officeObj);

        // DB query
        office.save(function(err) {
            if (err) {
                console.log(err);
                res.sendStatus(400);
            }
            else {
                res.sendStatus(200);
            }
        });
    }
    else {
        res.sendStatus(401);
    }
}

function editOffice(req, res, next) {
    if (req.session.passport.user) {
        var id = req.params.bid;
        // Update collection
        Office.update({
            '_id': id
        }, {
            $set: {
                "name": req.body.name,
                "description": req.body.description,
                "location": req.body.location,
                "email": req.body.email
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else res.sendStatus(200);
        });
    }
    else res.sendStatus(401);
}

function deleteOffice(req, res, next) {
    console.log(req.params.bid);
    if (req.session.passport.user) {
        var id = req.params.bid;

        Office.remove({
            '_id': id
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else{
                res.sendStatus(200);
            }
        });
    }
    else res.sendStatus(401);
}

module.exports.getOfficeForOrganization = getOfficeForOrganization;
module.exports.getAllOffices = getAllOffices;
module.exports.postOfficeAdd = postOfficeAdd;
module.exports.deleteOffice = deleteOffice;
module.exports.editOffice = editOffice;
module.exports.searchOffices = searchOffices;