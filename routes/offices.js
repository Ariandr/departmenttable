var express = require('express');
var router = express.Router();
var offices = require('../routes/officesLogics');

router.get('/getfororganization/:bid', offices.getOfficeForOrganization);
router.get('/getall', offices.getAllOffices);
router.get('/search/:query', offices.searchOffices);
router.post('/add', offices.postOfficeAdd);
router.delete('/delete/:bid', offices.deleteOffice);
router.put('/edit/:bid', offices.editOffice);

module.exports = router;