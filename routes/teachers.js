var express = require('express');
var router = express.Router();
var teachers = require('../routes/teacherLogics');

router.get('/getforoffice/:bid', teachers.getTeacherForOffice);
router.get('/getall', teachers.getAllTeachers);
router.get('/search/:query', teachers.searchTeachers);
router.get('/getbyid/:bid', teachers.getTeacherById);
router.post('/add', teachers.postTeacherAdd);
router.delete('/delete/:bid', teachers.deleteTeacher);
router.put('/edit/:bid', teachers.editTeacher);
router.put('/editbyteacher', teachers.editByTeacher);
router.post('/setnotes', teachers.setNotes);
router.post('/setstatus', teachers.setStatus);
router.post('/settime', teachers.setTime);
router.post('/login', teachers.login);

module.exports = router;