var db = require('../config/configurator.js');

var Organization = db.Organization;
var Office = db.Office;

function getAllOrganizations(req, res, next) {
    Organization.find({}, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc);
        }
    });
}

function postOrganizationAdd(req, res, next) {
    if (req.session.passport.user) {
        console.log(req.body.name);
        console.log(req.body.description);
        console.log(req.body.location);
        console.log(req.body.email);
        var organizationObj = {
            name: req.body.name,
            description: req.body.description,
            location: req.body.location,
            email: req.body.email
        };
        
        var organization = new Organization(organizationObj);

        // DB query
        organization.save(function(err) {
            if (err) {
                console.log(err);
                res.sendStatus(400);
            }
            else {
                res.sendStatus(200);
            }
        });
    }
    else {
        res.sendStatus(401);
    }
}

function editOrganization(req, res, next) {
    if (req.session.passport.user) {
        var id = req.params.bid;
        // Update collection
        Organization.update({
            '_id': id
        }, {
            $set: {
                "name": req.body.name,
                "description": req.body.description,
                "location": req.body.location,
                "email": req.body.email
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else res.sendStatus(200);
        });
    }
    else res.sendStatus(401);
}

function deleteOrganization(req, res, next) {
    if (req.session.passport.user) {
        var id = req.params.bid;

        Organization.remove({
            '_id': id
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                Office.remove({
                    organizationId: id
                }, function(err) {
                    if (err) res.sendStatus(400);
                    else {
                        res.sendStatus(200);
                    }
                });
            }
        });
    }
    else res.sendStatus(401);
}

module.exports.postOrganizationAdd = postOrganizationAdd;
module.exports.getAllOrganizations = getAllOrganizations;
module.exports.deleteOrganization = deleteOrganization;
module.exports.editOrganization = editOrganization;