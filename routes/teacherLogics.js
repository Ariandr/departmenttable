var db = require('../config/configurator.js');
var nodemailer = require('nodemailer');
var generatePassword = require("password-generator");

var Teacher = db.Teacher;

function getAllTeachers(req, res, next) {
    Teacher.find({}, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc);
        }
    });
}

function searchTeachers(req, res, next) {
    Teacher.find({}, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            var search = req.params.query.toLowerCase();
            var results = [];
            for (var i = 0; i < doc.length; i++) {
                if (doc[i].firstName.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].lastName.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].email.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].subject.toLowerCase().indexOf(search) >= 0 ||
                    doc[i].degree.toLowerCase().indexOf(search) >= 0) {
                    results.push(doc[i]);
                }
            }
            res.send(results);
        }
    });
}

function getTeacherForOffice(req, res, next) {
    var id = req.params.bid;
    Teacher.find({
        'officeId': id
    }, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc.reverse());
        }
    });
}

function getTeacherById(req, res, next) {
    var id = req.params.bid;
    Teacher.find({
        '_id': id
    }, function(err, doc) {
        if (err || !doc) res.sendStatus(400);
        else {
            res.send(doc);
        }
    });
}

function postTeacherAdd(req, res, next) {
    if (req.session.passport.user) {

        Teacher.find({
            'email': req.body.email
        }, function(err, doc) {
            if (err) {
                console.log(err);
                res.sendStatus(400);
            }
            else {
                if (doc.length < 1) {
                    var password = generatePassword(11, false);

                    var teacherObj = {
                        email: req.body.email,
                        password: password,
                        firstName: req.body.firstName,
                        lastName: req.body.lastName,
                        middleName: req.body.middleName,
                        degree: req.body.degree,
                        subject: req.body.subject,
                        officeId: req.body.officeId,
                        officeName: req.body.officeName,
                        location: req.body.location
                    };

                    var transporter = nodemailer.createTransport({
                        service: 'Gmail',
                        auth: {
                            user: 'fwocenter@gmail.com',
                            pass: 'dtpassword'
                        }
                    });

                    var mailOptions = {
                        from: 'DepartmentTable admin <fwocenter@gmail.com>', // sender address
                        to: req.body.email, // list of receivers
                        subject: 'Your password for DepartmentTable!', // Subject line
                        text: 'Hello, this is your password for Log in', // plaintext body
                        html: '<b>Your password for Log in as teacher: <H3>' + password + '</H3> <br> Don\'t show this password to anyone!</b><br><br><a href="https://department-table-ariandr.c9users.io">Visit our site</a>' // html body
                    };

                    // send mail with defined transport object
                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            res.sendStatus(400);
                            return;
                        }
                        var teacher = new Teacher(teacherObj);
                        // DB query
                        teacher.save(function(err) {
                            if (err) {
                                console.log(err);
                                res.sendStatus(400);
                            }
                            else {
                                res.sendStatus(200);
                            }
                        });
                    });
                }
                else {
                    res.sendStatus(406);
                }
            }
        });
    }
    else {
        res.sendStatus(401);
    }
}

function editTeacher(req, res, next) {
    if (req.session.passport.user) {
        var id = req.params.bid;
        // Update collection
        Teacher.update({
            '_id': id
        }, {
            $set: {
                'firstName': req.body.firstName,
                'lastName': req.body.lastName,
                'middleName': req.body.middleName,
                'degree': req.body.degree,
                'subject': req.body.subject
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else res.sendStatus(200);
        });
    }
    else res.sendStatus(401);
}

function editByTeacher(req, res, next) {
    if (req.body.email.length > 0 && req.body.password.length > 0) {
        // Update collection
        Teacher.update({
            'email': req.body.email,
            'password': req.body.password
        }, {
            $set: {
                'firstName': req.body.firstName,
                'lastName': req.body.lastName,
                'middleName': req.body.middleName,
                'degree': req.body.degree,
                'subject': req.body.subject,
                'location': req.body.location
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else res.sendStatus(200);
        });
    }
    else res.sendStatus(401);
}

function deleteTeacher(req, res, next) {
    if (req.session.passport.user) {
        var id = req.params.bid;

        Teacher.remove({
            '_id': id
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else
                res.sendStatus(200);
        });
    }
    else res.sendStatus(401);
}

function setNotes(req, res, next) {
    if (req.body.email.length > 0 && req.body.password.length > 0) {
        // Update collection
        Teacher.update({
            'email': req.body.email,
            'password': req.body.password
        }, {
            $set: {
                'notes': req.body.notes,
                'lastSeen': Date.now()
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                Teacher.find({
                    'email': req.body.email,
                    'password': req.body.password
                }, function(err, doc) {
                    if (err || !doc) res.sendStatus(400);
                    else res.send(doc[0]);
                });
            }
        });
    }
    else res.sendStatus(401);
}

function setStatus(req, res, next) {
    if (req.body.email.length > 0 && req.body.password.length > 0) {
        // Update collection
        Teacher.update({
            'email': req.body.email,
            'password': req.body.password
        }, {
            $set: {
                'status': req.body.status,
                'lastSeen': Date.now()
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                Teacher.find({
                    'email': req.body.email,
                    'password': req.body.password
                }, function(err, doc) {
                    if (err || !doc) res.sendStatus(400);
                    else res.send(doc[0]);
                });
            }
        });
    }
    else res.sendStatus(401);
}

function login(req, res, next) {
    if (req.body.email.length > 0 && req.body.password.length > 0) {
        // Update collection
        Teacher.find({
            'email': req.body.email,
            'password': req.body.password
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else res.send(doc[0]);
        });
    }
    else res.sendStatus(401);
}

function setTime(req, res, next) {
    if (req.body.email.length > 0 && req.body.password.length > 0) {
        // Update collection
        Teacher.update({
            'email': req.body.email,
            'password': req.body.password
        }, {
            $set: {
                'lastSeen': Date.now()
            }
        }, function(err, doc) {
            if (err || !doc) res.sendStatus(400);
            else {
                Teacher.find({
                    'email': req.body.email,
                    'password': req.body.password
                }, function(err, doc) {
                    if (err || !doc) res.sendStatus(400);
                    else res.send(doc[0]);
                });
            }
        });
    }
    else res.sendStatus(401);
}

module.exports.getAllTeachers = getAllTeachers;
module.exports.getTeacherForOffice = getTeacherForOffice;
module.exports.postTeacherAdd = postTeacherAdd;
module.exports.deleteTeacher = deleteTeacher;
module.exports.editTeacher = editTeacher;
module.exports.setNotes = setNotes;
module.exports.setStatus = setStatus;
module.exports.setTime = setTime;
module.exports.searchTeachers = searchTeachers;
module.exports.editByTeacher = editByTeacher;
module.exports.login = login;
module.exports.getTeacherById = getTeacherById;