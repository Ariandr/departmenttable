var express = require('express');
var router = express.Router();
var organizations = require('../routes/organizationLogics');

router.post('/add', organizations.postOrganizationAdd);
router.get('/getall', organizations.getAllOrganizations);
router.delete('/delete/:bid', organizations.deleteOrganization);
router.put('/edit/:bid', organizations.editOrganization);

module.exports = router;