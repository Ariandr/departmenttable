var dbConfig = require('./db.config.js');
var authConfig = require('./auth.config.js');

var environment = process.env.NODE_ENV;
var strategies, connection;

switch (environment) {
    case 'dev':
        strategies = authConfig.development;
        connection = dbConfig.development;
        break;
    case 'prod':
        strategies = authConfig.production;
        connection = dbConfig.production;
        break;
    default:
        strategies = authConfig.development;
        connection = dbConfig.development;
        break;
}

var db = require("mongoose").connect(connection);
var userSchema = require('../models/user.js').UserSchema;
var organizationSchema = require('../models/user.js').OrganizationSchema;
var officeSchema = require('../models/user.js').OfficeSchema;
var teacherSchema = require('../models/user.js').TeacherSchema;

var user = db.model('users', userSchema);
var organization = db.model('organizations', organizationSchema);
var office = db.model('offices', officeSchema);
var teacher = db.model('teachers', teacherSchema);

module.exports.strategies = strategies;
module.exports.User = user;
module.exports.Organization = organization;
module.exports.Office = office;
module.exports.Teacher = teacher;