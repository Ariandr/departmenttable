var mongoose = require('mongoose');

var teacherSchema = new mongoose.Schema({
   email: String,
   password: String,
   firstName: String,
   lastName: String,
   middleName: String,
   degree: String,
   subject: String,
   status: {
      type: Boolean,
      default: false
   },
   lastSeen: {
      type: Number,
      default: 0
   },
   notes: {
      type: String,
      default: "No information!"
   },
   officeId: mongoose.Schema.Types.ObjectId,
   officeName: String,
   location: String
});

var officeSchema = new mongoose.Schema({
   name: String,
   location: String,
   description: String,
   organizationId: mongoose.Schema.Types.ObjectId,
   email: String
});

var organizationSchema = new mongoose.Schema({
   name: String,
   description: String,
   location: String,
   email: String
});

var userSchema = new mongoose.Schema({
   oauth_id: String,
   name: String,
   email: String
});

exports.UserSchema = userSchema;
exports.OrganizationSchema = organizationSchema;
exports.OfficeSchema = officeSchema;
exports.TeacherSchema = teacherSchema;